﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Globalization;

namespace DAM.ToDo
{
    public static class ToDoData
    {
        //Recogemos la ruta del archivo de configuración
        private static string DataPath = System.Configuration.ConfigurationManager.AppSettings["DataPath"];

        public static void Save(ToDo t)
        {
            string fileName = t.Created.ToString("yyyyMMdd_hhmmss_FFFFFFF");
            string path = DataPath + fileName;

            using (FileStream fs = File.Create(path))
            {

            }


            using (StreamWriter sw = File.CreateText(path))
            {
                sw.WriteLine(t.Task);
                sw.WriteLine(t.Priority.GetHashCode());

                if(t.Done.HasValue)
                {
                    sw.WriteLine(t.Done.Value.ToString("yyyy-MM-ddThh:mm:ss.FFFFFFF"));
                }
                else
                {
                    sw.WriteLine();
                }
                if (t.Schedule.HasValue)
                {
                    sw.WriteLine(t.Schedule.Value.ToString("yyyy-MM-ddThh:mm:ss.FFFFFFF"));
                }
                else
                {
                    sw.WriteLine();
                }
                sw.WriteLine(t.Details);
            }


        }

        public static List<ToDo> Select()
        {
            List<ToDo> tareas = new List<ToDo>();

            var files = Directory.GetFiles(DataPath);

            foreach (var file in files)
            {
                string filename = Path.GetFileName(file);
                DateTime created;
                if (DateTime.TryParseExact(filename, "yyyyMMdd_hhmmss_FFFFFFF", CultureInfo.InvariantCulture, DateTimeStyles.None, out created))
                {
                    string[] arrayLineas = File.ReadAllLines(file);
                    string task = arrayLineas[0];
                    ToDoPriority prioridad = (ToDoPriority)Enum.Parse(typeof(ToDoPriority), arrayLineas[1]);
                    DateTime? done = null;

                    if (arrayLineas[2].Length > 0)
                    {
                        done = DateTime.ParseExact(arrayLineas[2], "yyyy-MM-ddThh:mm:ss.FFFFFFF", CultureInfo.InvariantCulture);
                    }

                    DateTime? schedule = null;

                    if (arrayLineas[3].Length > 0)
                    {
                        schedule = DateTime.ParseExact(arrayLineas[2], "yyyy-MM-ddThh:mm:ss.FFFFFFF", CultureInfo.InvariantCulture);
                    }

                    string details = String.Empty;

                    if (arrayLineas[4].Length > 0)
                    {
                        details = arrayLineas[4];
                    }
                    ToDo toDo = new ToDo(task, details, created, prioridad, done, schedule);
                    tareas.Add(toDo);
                }
            }

            return tareas;
        }

        public static void Delete(ToDo param)
        {
            string fileName = param.Created.ToString("yyyyMMdd_hhmmss_FFFFFFF");
            string ruta = DataPath + fileName;

            File.Delete(ruta);

        }
    }
}
