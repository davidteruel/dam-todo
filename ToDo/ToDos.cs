﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;

namespace DAM.ToDo
{
    public class ToDos
    {
        public List<ToDo> lst;

        public ToDos()
        {
            lst = ToDoData.Select();
        }

        public ToDo[] GetPending()
        {
            ToDo[] arrayToDos = new ToDo[lst.Count];
            int contador = 0;

            foreach (ToDo i in lst)
            {
                if (!i.Done.HasValue)
                {
                    arrayToDos[contador] = i;
                    contador++;
                }
            }

            return arrayToDos;
        }

        public ToDo[] GetDone()
        {
            ToDo[] arrayToDosDone = new ToDo[lst.Count];
            int contador = 0;

            foreach (ToDo i in lst)
            {
                if (i.Done.HasValue)
                {
                    arrayToDosDone[contador] = i;
                    contador++;
                }
            }

            return arrayToDosDone;
        }

        public void Add(string s)
        {
            ToDo tarea = new ToDo(s);
            lst.Add(tarea);
            ToDoData.Save(tarea);
        }

        public bool Remove(ToDo t)
        {
            if (lst.Remove(t))
            {
                ToDoData.Delete(t);
                return true;
            }
            else
            {
                return false;
            }
        }
            

        public void Do(ToDo t)
        {
            if (!t.Done.HasValue)
            {
                t.Done = DateTime.Now;
            }
            ToDoData.Save(t);
        }

        public void UnDo(ToDo t)
        {

            if (t.Done.HasValue)
            {
                t.Done = null;
            }
            ToDoData.Save(t);
        }
    }
}
