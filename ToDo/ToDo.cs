﻿using System;
using System.Collections.Generic;


namespace DAM.ToDo
{
    public enum ToDoPriority { Regular, Important, Urgent };
   

    public class ToDo
    {
        public string Task { get; set; }
        public string Details { get; set; }
        public DateTime Created { get; private set; }
        public ToDoPriority Priority { get; set; }
        public DateTime? Done { get; internal set; }
        public DateTime? Schedule { get; set; }

        public ToDo(string task)
        {
            this.Task = task;
            this.Priority = ToDoPriority.Regular;
            this.Created = DateTime.Now;
        }

        public ToDo(string task, string details, DateTime created, ToDoPriority priority, DateTime? done, DateTime? schedule)
        {
            this.Task = task;
            this.Details = details;
            this.Created = created;
            this.Priority = priority;
            this.Done = done;
            this.Schedule = schedule;
        }
    }
}
