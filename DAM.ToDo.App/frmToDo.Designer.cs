﻿namespace DAM.ToDo.App
{
    partial class frmToDo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lblDone = new System.Windows.Forms.Label();
            this.txtDone = new System.Windows.Forms.TextBox();
            this.txtCreated = new System.Windows.Forms.TextBox();
            this.dtpSchedule = new System.Windows.Forms.DateTimePicker();
            this.lblCreated = new System.Windows.Forms.Label();
            this.lblPriority = new System.Windows.Forms.Label();
            this.lblTask = new System.Windows.Forms.Label();
            this.txtTask = new System.Windows.Forms.TextBox();
            this.lblSchedule = new System.Windows.Forms.Label();
            this.cboPriority = new System.Windows.Forms.ComboBox();
            this.toDoBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDetails = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnDone = new System.Windows.Forms.Button();
            this.btnPending = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnQuit = new System.Windows.Forms.Button();
            this.toDoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.toDoBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toDoBindingSource1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toDoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toDoBindingSource2)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lblDone);
            this.splitContainer1.Panel1.Controls.Add(this.txtDone);
            this.splitContainer1.Panel1.Controls.Add(this.txtCreated);
            this.splitContainer1.Panel1.Controls.Add(this.dtpSchedule);
            this.splitContainer1.Panel1.Controls.Add(this.lblCreated);
            this.splitContainer1.Panel1.Controls.Add(this.lblPriority);
            this.splitContainer1.Panel1.Controls.Add(this.lblTask);
            this.splitContainer1.Panel1.Controls.Add(this.txtTask);
            this.splitContainer1.Panel1.Controls.Add(this.lblSchedule);
            this.splitContainer1.Panel1.Controls.Add(this.cboPriority);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Size = new System.Drawing.Size(376, 340);
            this.splitContainer1.SplitterDistance = 144;
            this.splitContainer1.TabIndex = 0;
            // 
            // lblDone
            // 
            this.lblDone.AutoSize = true;
            this.lblDone.Location = new System.Drawing.Point(225, 63);
            this.lblDone.Name = "lblDone";
            this.lblDone.Size = new System.Drawing.Size(39, 13);
            this.lblDone.TabIndex = 9;
            this.lblDone.Text = "Hecha";
            // 
            // txtDone
            // 
            this.txtDone.Location = new System.Drawing.Point(282, 60);
            this.txtDone.Name = "txtDone";
            this.txtDone.Size = new System.Drawing.Size(84, 20);
            this.txtDone.TabIndex = 8;
            // 
            // txtCreated
            // 
            this.txtCreated.Location = new System.Drawing.Point(84, 78);
            this.txtCreated.Name = "txtCreated";
            this.txtCreated.Size = new System.Drawing.Size(121, 20);
            this.txtCreated.TabIndex = 7;
            // 
            // dtpSchedule
            // 
            this.dtpSchedule.Location = new System.Drawing.Point(84, 108);
            this.dtpSchedule.Name = "dtpSchedule";
            this.dtpSchedule.Size = new System.Drawing.Size(224, 20);
            this.dtpSchedule.TabIndex = 6;
            // 
            // lblCreated
            // 
            this.lblCreated.AutoSize = true;
            this.lblCreated.Location = new System.Drawing.Point(12, 81);
            this.lblCreated.Name = "lblCreated";
            this.lblCreated.Size = new System.Drawing.Size(41, 13);
            this.lblCreated.TabIndex = 5;
            this.lblCreated.Text = "Creada";
            // 
            // lblPriority
            // 
            this.lblPriority.AutoSize = true;
            this.lblPriority.Location = new System.Drawing.Point(12, 46);
            this.lblPriority.Name = "lblPriority";
            this.lblPriority.Size = new System.Drawing.Size(48, 13);
            this.lblPriority.TabIndex = 4;
            this.lblPriority.Text = "Prioridad";
            // 
            // lblTask
            // 
            this.lblTask.AutoSize = true;
            this.lblTask.Location = new System.Drawing.Point(12, 15);
            this.lblTask.Name = "lblTask";
            this.lblTask.Size = new System.Drawing.Size(35, 13);
            this.lblTask.TabIndex = 3;
            this.lblTask.Text = "Tarea";
            // 
            // txtTask
            // 
            this.txtTask.Location = new System.Drawing.Point(84, 12);
            this.txtTask.Name = "txtTask";
            this.txtTask.Size = new System.Drawing.Size(282, 20);
            this.txtTask.TabIndex = 2;
            // 
            // lblSchedule
            // 
            this.lblSchedule.AutoSize = true;
            this.lblSchedule.Location = new System.Drawing.Point(11, 114);
            this.lblSchedule.Name = "lblSchedule";
            this.lblSchedule.Size = new System.Drawing.Size(64, 13);
            this.lblSchedule.TabIndex = 1;
            this.lblSchedule.Text = "Programada";
            // 
            // cboPriority
            // 
            this.cboPriority.DataSource = this.toDoBindingSource1;
            this.cboPriority.FormattingEnabled = true;
            this.cboPriority.Location = new System.Drawing.Point(84, 43);
            this.cboPriority.Name = "cboPriority";
            this.cboPriority.Size = new System.Drawing.Size(121, 21);
            this.cboPriority.TabIndex = 0;
            // 
            // toDoBindingSource1
            // 
            this.toDoBindingSource1.DataSource = typeof(DAM.ToDo.ToDo);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDetails);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(375, 189);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalles";
            // 
            // txtDetails
            // 
            this.txtDetails.Location = new System.Drawing.Point(6, 16);
            this.txtDetails.Multiline = true;
            this.txtDetails.Name = "txtDetails";
            this.txtDetails.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDetails.Size = new System.Drawing.Size(363, 167);
            this.txtDetails.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnDone);
            this.flowLayoutPanel1.Controls.Add(this.btnPending);
            this.flowLayoutPanel1.Controls.Add(this.btnDelete);
            this.flowLayoutPanel1.Controls.Add(this.btnOK);
            this.flowLayoutPanel1.Controls.Add(this.btnQuit);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 343);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(375, 33);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // btnDone
            // 
            this.btnDone.Location = new System.Drawing.Point(3, 3);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(57, 23);
            this.btnDone.TabIndex = 0;
            this.btnDone.Text = "Hecha";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.bntDone_Click);
            // 
            // btnPending
            // 
            this.btnPending.Location = new System.Drawing.Point(66, 3);
            this.btnPending.Name = "btnPending";
            this.btnPending.Size = new System.Drawing.Size(69, 23);
            this.btnPending.TabIndex = 1;
            this.btnPending.Text = "Pendiente";
            this.btnPending.UseVisualStyleBackColor = true;
            this.btnPending.Click += new System.EventHandler(this.btnPending_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(141, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(61, 23);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Borrar";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.bntDelete_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(208, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(68, 23);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "Aceptar";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnQuit
            // 
            this.btnQuit.Location = new System.Drawing.Point(282, 3);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(63, 23);
            this.btnQuit.TabIndex = 4;
            this.btnQuit.Text = "Salir";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // toDoBindingSource
            // 
            this.toDoBindingSource.DataSource = typeof(DAM.ToDo.ToDo);
            // 
            // toDoBindingSource2
            // 
            this.toDoBindingSource2.DataSource = typeof(DAM.ToDo.ToDo);
            // 
            // frmToDo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 378);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frmToDo";
            this.Text = "Detalles Tarea";
            this.Load += new System.EventHandler(this.frmToDo_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.toDoBindingSource1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.toDoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toDoBindingSource2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lblDone;
        private System.Windows.Forms.TextBox txtDone;
        private System.Windows.Forms.TextBox txtCreated;
        private System.Windows.Forms.DateTimePicker dtpSchedule;
        private System.Windows.Forms.Label lblCreated;
        private System.Windows.Forms.Label lblPriority;
        private System.Windows.Forms.Label lblTask;
        private System.Windows.Forms.TextBox txtTask;
        private System.Windows.Forms.Label lblSchedule;
        private System.Windows.Forms.ComboBox cboPriority;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtDetails;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Button btnPending;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.BindingSource toDoBindingSource1;
        private System.Windows.Forms.BindingSource toDoBindingSource;
        private System.Windows.Forms.BindingSource toDoBindingSource2;
    }
}