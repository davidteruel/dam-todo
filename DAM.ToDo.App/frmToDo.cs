﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAM.ToDo.App
{
    public partial class frmToDo : Form
    {
        //PROPIEDAD ToDo de tipo ToDo
        public ToDo ToDo { get; set; }

        //ENUMERACION Actions para los distintas acciones de los buttons del formulario.
        public enum Actions { Done, Delete, OK, Quit };

        //variable action para la propiedad Action
        private Actions action; 
        //PROPIEDAD Action
        public Actions Action
        {
            get
            {
                return action;
            }

            private set
            {
                action = value;
            }
        }

        public frmToDo()
        {
            InitializeComponent();
        }


        //Metodo para el click sobre el boton aceptar en este formulario.
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (txtTask.Text != null)
            {
                ToDo.Task = txtTask.Text;
                ToDo.Priority = (ToDoPriority)cboPriority.SelectedItem;
                ToDo.Schedule = dtpSchedule.Value;
                ToDo.Details = txtDetails.Text;
                this.action = Actions.OK;
                this.Close();
                
            }
            else
            {
                MessageBox.Show("El campo Tarea esta vacío.");
            }

        }
        //Metodo para el click sobre el boton Borrar en este formulario.
        private void bntDelete_Click(object sender, EventArgs e)
        {
            DialogResult oDlgRes;
            oDlgRes = MessageBox.Show("¿Está seguro que desea eliminar el registro seleccionado ?", "Confirmación",
            MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (oDlgRes == DialogResult.Yes)
            {
                this.action = Actions.Delete;
                this.Close();
            }
        }
        //Metodo para el click sobre el boton hecha en este formulario.
        private void bntDone_Click(object sender, EventArgs e)
        {
            //si txtTask contiene valor, los campos del objeto se actualizan y se asigna Actions.OK a la accion.
            if (txtTask.Text != null)
            {
                ToDo.Task = txtTask.Text;
                ToDo.Priority = (ToDoPriority)cboPriority.SelectedItem;
                ToDo.Schedule = dtpSchedule.Value;
                ToDo.Details = txtDetails.Text;
                this.action = Actions.OK;
                this.Close();

            }
            //Si txtTask esta vacio se lanza este mensaje y no se ejecuta ninguna accion.
            else
            {
                MessageBox.Show("El campo Tarea esta vacío.");
            }
        }

        private void frmToDo_Load(object sender, EventArgs e)
        {
            //Al cargar el formulario se carga tambien el menu desplegable de la Enumeracion ToDoPriority
            cboPriority.DataSource = Enum.GetValues(typeof(ToDoPriority));
            txtTask.Text = ToDo.Task;
        }

        //Metodo para el click sobre el boton Salir en este formulario.
        private void btnQuit_Click(object sender, EventArgs e)
        {
            //Se asigna el valor Action.Quit a la accion y sale del subformulario.
            this.action = Actions.Quit;
            this.Close();
        }

        //Metodo para el click sobre el boton Pendiente en este formulario.
        private void btnPending_Click(object sender, EventArgs e)
        {
            //Se ocultan tanto lblDone como txtDone.
            lblDone.Visible = false;
            txtDone.Visible = false;
        }
    }
}
