﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAM.ToDo.App
{
    public partial class frmToDos : Form
    {
        //objeto de tipo ToDos t1
        ToDos t1 = new ToDos();

        //Metodo que refresca la lstPending en caso de cambios, tambien se actualiza el texto de lblPending.
        public void RefreshPendingData()
        {
            lstPending.DataSource = null;
            lstPending.DataSource = t1.GetPending();
            lstPending.DisplayMember = "Task";
            lblPending.Text = "## TAREAS PENDIENTES " + lstPending.Items.Count;
        }
        //Metodo que refresca la lstDone en caso de cambios, tambien se actualiza el texto de lblPending.
        public void RefreshDoneData()
        {
            lstDone.DataSource = null;
            lstDone.DataSource = t1.GetDone();
            lstDone.DisplayMember = "Task";
            lblDone.Text = "## TAREAS HECHAS " + lstDone.Items.Count;
        }
        public frmToDos()
        {
            InitializeComponent();
        }

        private void frmToDos_Load(object sender, EventArgs e)
        {
            //Creamos 3 tareas para que sean cargadas al iniciar la app y marcamos una como hecha.

            /*t1.Add("hacer la compra");
            t1.Add("hacer la comida");
            t1.Add("pasear al perro");

            t1.Do(t1.lst[1]);*/

            /*Al cargar se llama a los metodos RefreshPendingData y RefreshDoneData.
            Se actualiza el valor del sbStatus.*/

            RefreshPendingData();
            RefreshDoneData();

            sbStatus.Text = "LAS TAREAS SE HAN CARGADO CORRECTAMENTE.";
        }

        //Metodo para el click sobre el button Crear en este formulario.
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            //Si txtTask no esta vacio se añade la nueva tarea para ser procesada luego.
            if (txtTask.Text != null)
            {
                //Se eliminan espacios innecesarios con Trim.
                t1.Add(txtTask.Text.Trim());
                RefreshPendingData();
                txtTask.Clear();
                /*lstPending.SelectedItem;*/
                
                //Si se añade una tarea se actualiza sbStatus.
                sbStatus.Text = "Se ha añadido una nueva tarea.";
            }
            //Si txtTask esta vacio se lanza un messageBox advirtiendo del error.
            else
            {
                MessageBox.Show("El campo esta vacío, no se puede añadir ninguna tarea.");
            }
        }

        //MENU STRIP PESTAÑA PENDING.

        //Metodo para el click sobre Hecha en el menu desplegable.
        private void HechaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Se guarda el index en la variable index para luego comprobar. 
            int index = lstPending.SelectedIndex;
            //Se crea un ToDo tarea haciendo cast del elemento seleccionado de lstPending.
            ToDo tarea = (ToDo)lstPending.SelectedItem;
            //Si es mayor que cero se marca como hecha la tarea.
            if (index >= 0)
            {
                t1.Do(tarea);
            }
            //se refrescan los datos y se actualiza el mensaje del sbstatus.
            RefreshDoneData();
            RefreshPendingData();
            sbStatus.Text = "Se ha marcado la tarea como hecha.";

        }

        private void EliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int index = lstPending.SelectedIndex;
            ToDo tarea = (ToDo)lstPending.SelectedItem;

            sbStatus.Text = "Se ha eliminado la tarea correctamente.";
        }

        private void detallesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int index = lstPending.SelectedIndex;
            if (index >= 0)
            {
                ToDo t = (ToDo)lstPending.Items[index];
                ShowDetails(t);
            }
        }

        //MENU STRIP PESTAÑA DONE.

        //Metodo para el click sobre Pendiente en el menu desplegable. 
        //Mismo procedimiento que en Pending
        private void PendienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int index = lstDone.SelectedIndex;
            ToDo tarea = (ToDo)lstDone.SelectedItem;

            if (index >= 0)
            {
                t1.UnDo(tarea);
            }
            RefreshDoneData();
            RefreshPendingData();
            sbStatus.Text = "Se ha marcado la tarea como pendiente.";
        }

        //Metodo para el click sobre la opcion eliminar.
        private void EliminarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            //se selecciona el indice.
            int index = lstDone.SelectedIndex;
            //objeto DialogResult 
            DialogResult oDlgRes;
            //Se lanza un message box con las opciones si y no 
            oDlgRes = MessageBox.Show("¿Está seguro que desea eliminar el registro seleccionado ?", "Confirmación",
            MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            //En caso de si se elimina de la lista el indice.
            if (oDlgRes == DialogResult.Yes)
            {
                lstDone.Items.RemoveAt(index);
                sbStatus.Text = "Se ha eliminado la tarea correctamente.";
            }
            
        }



        private void ShowDetails(ToDo t)
        {
            frmToDo frm = new frmToDo();
            frm.ToDo = t;
            frm.ShowDialog();
            var action = frm.Action;
            switch (action)
            {
                case frmToDo.Actions.Done:
                    t1.Do(t);
                    sbStatus.Text = "Tarea hecha";
                    break;
                case frmToDo.Actions.Delete:
                    sbStatus.Text = "Tarea borrada";
                    t1.Remove(t);
                    break;
                case frmToDo.Actions.OK:
                    sbStatus.Text = "Tarea guardada";
                    break;
                case frmToDo.Actions.Quit:
                    break;
                default:
                    break;
            }
            RefreshPendingData();
            RefreshDoneData();
        }

        private void detallesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            int index = lstDone.SelectedIndex;
            if (index >= 0)
            {
                ToDo t = (ToDo)lstDone.Items[index];
                ShowDetails(t);
            }
        }

        private void lstDone_DoubleClick(object sender, EventArgs e)
        {
            int index = lstDone.SelectedIndex;
            if (index >= 0)
            {
                ToDo t = (ToDo)lstDone.Items[index];
                ShowDetails(t);
            }
            
        }

        private void lstPending_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = lstPending.IndexFromPoint(e.Location);
            ToDo t = (ToDo)lstPending.Items[index];
            if (index >= 0)
            {
                t1.Do(t);
                sbStatus.Text = "La tarea se ha marcado como hecha.";
            }

            RefreshDoneData();
            RefreshPendingData();
        }

        private void lstDone_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = lstDone.IndexFromPoint(e.Location);
            ToDo t = (ToDo)lstDone.Items[index];
            ShowDetails(t);

        }
    }
}