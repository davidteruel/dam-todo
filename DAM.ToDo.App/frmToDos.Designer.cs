﻿namespace DAM.ToDo.App
{
    partial class frmToDos
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabToDo = new System.Windows.Forms.TabControl();
            this.tabPending = new System.Windows.Forms.TabPage();
            this.grbNewTask = new System.Windows.Forms.GroupBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtTask = new System.Windows.Forms.TextBox();
            this.panPending = new System.Windows.Forms.Panel();
            this.lblPending = new System.Windows.Forms.Label();
            this.lstPending = new System.Windows.Forms.ListBox();
            this.mnuPending = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.detallesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.hechaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.eliminarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabDone = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDone = new System.Windows.Forms.Label();
            this.lstDone = new System.Windows.Forms.ListBox();
            this.mnuDone = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.detallesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.pendienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.eliminarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.sbStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabToDo.SuspendLayout();
            this.tabPending.SuspendLayout();
            this.grbNewTask.SuspendLayout();
            this.panPending.SuspendLayout();
            this.mnuPending.SuspendLayout();
            this.tabDone.SuspendLayout();
            this.panel1.SuspendLayout();
            this.mnuDone.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabToDo
            // 
            this.tabToDo.Controls.Add(this.tabPending);
            this.tabToDo.Controls.Add(this.tabDone);
            this.tabToDo.Location = new System.Drawing.Point(2, 1);
            this.tabToDo.Name = "tabToDo";
            this.tabToDo.SelectedIndex = 0;
            this.tabToDo.Size = new System.Drawing.Size(264, 362);
            this.tabToDo.TabIndex = 1;
            // 
            // tabPending
            // 
            this.tabPending.Controls.Add(this.grbNewTask);
            this.tabPending.Controls.Add(this.panPending);
            this.tabPending.Controls.Add(this.lstPending);
            this.tabPending.Location = new System.Drawing.Point(4, 22);
            this.tabPending.Name = "tabPending";
            this.tabPending.Padding = new System.Windows.Forms.Padding(3);
            this.tabPending.Size = new System.Drawing.Size(256, 336);
            this.tabPending.TabIndex = 0;
            this.tabPending.Text = "Pending";
            this.tabPending.UseVisualStyleBackColor = true;
            // 
            // grbNewTask
            // 
            this.grbNewTask.Controls.Add(this.btnAdd);
            this.grbNewTask.Controls.Add(this.txtTask);
            this.grbNewTask.Location = new System.Drawing.Point(6, 3);
            this.grbNewTask.Name = "grbNewTask";
            this.grbNewTask.Size = new System.Drawing.Size(247, 51);
            this.grbNewTask.TabIndex = 2;
            this.grbNewTask.TabStop = false;
            this.grbNewTask.Text = "Nueva Tarea";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(173, 16);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(71, 23);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "Crear";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // txtTask
            // 
            this.txtTask.Location = new System.Drawing.Point(6, 19);
            this.txtTask.Name = "txtTask";
            this.txtTask.Size = new System.Drawing.Size(161, 20);
            this.txtTask.TabIndex = 0;
            // 
            // panPending
            // 
            this.panPending.Controls.Add(this.lblPending);
            this.panPending.Location = new System.Drawing.Point(6, 288);
            this.panPending.Name = "panPending";
            this.panPending.Size = new System.Drawing.Size(242, 42);
            this.panPending.TabIndex = 1;
            // 
            // lblPending
            // 
            this.lblPending.AutoSize = true;
            this.lblPending.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblPending.Location = new System.Drawing.Point(15, 10);
            this.lblPending.Name = "lblPending";
            this.lblPending.Size = new System.Drawing.Size(139, 13);
            this.lblPending.TabIndex = 0;
            this.lblPending.Text = "## TAREAS PENDIENTES";
            // 
            // lstPending
            // 
            this.lstPending.ContextMenuStrip = this.mnuPending;
            this.lstPending.FormattingEnabled = true;
            this.lstPending.Location = new System.Drawing.Point(0, 60);
            this.lstPending.Name = "lstPending";
            this.lstPending.Size = new System.Drawing.Size(256, 225);
            this.lstPending.TabIndex = 0;
            this.lstPending.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstPending_MouseDoubleClick);
            // 
            // mnuPending
            // 
            this.mnuPending.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.detallesToolStripMenuItem,
            this.toolStripMenuItem1,
            this.hechaToolStripMenuItem,
            this.toolStripMenuItem2,
            this.eliminarToolStripMenuItem});
            this.mnuPending.Name = "mnuPending";
            this.mnuPending.Size = new System.Drawing.Size(181, 104);
            // 
            // detallesToolStripMenuItem
            // 
            this.detallesToolStripMenuItem.Name = "detallesToolStripMenuItem";
            this.detallesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.detallesToolStripMenuItem.Text = "Detalles";
            this.detallesToolStripMenuItem.Click += new System.EventHandler(this.detallesToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(177, 6);
            // 
            // hechaToolStripMenuItem
            // 
            this.hechaToolStripMenuItem.Name = "hechaToolStripMenuItem";
            this.hechaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.hechaToolStripMenuItem.Text = "Hecha";
            this.hechaToolStripMenuItem.Click += new System.EventHandler(this.HechaToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(177, 6);
            // 
            // eliminarToolStripMenuItem
            // 
            this.eliminarToolStripMenuItem.Name = "eliminarToolStripMenuItem";
            this.eliminarToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.eliminarToolStripMenuItem.Text = "Eliminar";
            this.eliminarToolStripMenuItem.Click += new System.EventHandler(this.EliminarToolStripMenuItem_Click);
            // 
            // tabDone
            // 
            this.tabDone.Controls.Add(this.panel1);
            this.tabDone.Controls.Add(this.lstDone);
            this.tabDone.Location = new System.Drawing.Point(4, 22);
            this.tabDone.Name = "tabDone";
            this.tabDone.Padding = new System.Windows.Forms.Padding(3);
            this.tabDone.Size = new System.Drawing.Size(256, 336);
            this.tabDone.TabIndex = 1;
            this.tabDone.Text = "Done";
            this.tabDone.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblDone);
            this.panel1.Location = new System.Drawing.Point(6, 286);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(247, 32);
            this.panel1.TabIndex = 1;
            // 
            // lblDone
            // 
            this.lblDone.AutoSize = true;
            this.lblDone.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblDone.Location = new System.Drawing.Point(15, 9);
            this.lblDone.Name = "lblDone";
            this.lblDone.Size = new System.Drawing.Size(114, 13);
            this.lblDone.TabIndex = 0;
            this.lblDone.Text = "## TAREAS HECHAS";
            // 
            // lstDone
            // 
            this.lstDone.ContextMenuStrip = this.mnuDone;
            this.lstDone.FormattingEnabled = true;
            this.lstDone.Location = new System.Drawing.Point(3, 3);
            this.lstDone.Name = "lstDone";
            this.lstDone.Size = new System.Drawing.Size(253, 277);
            this.lstDone.TabIndex = 0;
            this.lstDone.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstDone_MouseDoubleClick);
            // 
            // mnuDone
            // 
            this.mnuDone.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.detallesToolStripMenuItem1,
            this.toolStripMenuItem3,
            this.pendienteToolStripMenuItem,
            this.toolStripMenuItem4,
            this.eliminarToolStripMenuItem1});
            this.mnuDone.Name = "mnuDone";
            this.mnuDone.Size = new System.Drawing.Size(128, 82);
            // 
            // detallesToolStripMenuItem1
            // 
            this.detallesToolStripMenuItem1.Name = "detallesToolStripMenuItem1";
            this.detallesToolStripMenuItem1.Size = new System.Drawing.Size(127, 22);
            this.detallesToolStripMenuItem1.Text = "Detalles";
            this.detallesToolStripMenuItem1.Click += new System.EventHandler(this.detallesToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(124, 6);
            // 
            // pendienteToolStripMenuItem
            // 
            this.pendienteToolStripMenuItem.Name = "pendienteToolStripMenuItem";
            this.pendienteToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.pendienteToolStripMenuItem.Text = "Pendiente";
            this.pendienteToolStripMenuItem.Click += new System.EventHandler(this.PendienteToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(124, 6);
            // 
            // eliminarToolStripMenuItem1
            // 
            this.eliminarToolStripMenuItem1.Name = "eliminarToolStripMenuItem1";
            this.eliminarToolStripMenuItem1.Size = new System.Drawing.Size(127, 22);
            this.eliminarToolStripMenuItem1.Text = "Eliminar";
            this.eliminarToolStripMenuItem1.Click += new System.EventHandler(this.EliminarToolStripMenuItem1_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sbStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 344);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(266, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // sbStatus
            // 
            this.sbStatus.Name = "sbStatus";
            this.sbStatus.Size = new System.Drawing.Size(118, 17);
            this.sbStatus.Text = "toolStripStatusLabel1";
            // 
            // frmToDos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(266, 366);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabToDo);
            this.Name = "frmToDos";
            this.Text = "Tareas";
            this.Load += new System.EventHandler(this.frmToDos_Load);
            this.tabToDo.ResumeLayout(false);
            this.tabPending.ResumeLayout(false);
            this.grbNewTask.ResumeLayout(false);
            this.grbNewTask.PerformLayout();
            this.panPending.ResumeLayout(false);
            this.panPending.PerformLayout();
            this.mnuPending.ResumeLayout(false);
            this.tabDone.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.mnuDone.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TabControl tabToDo;
        private System.Windows.Forms.TabPage tabPending;
        private System.Windows.Forms.TabPage tabDone;
        private System.Windows.Forms.ListBox lstPending;
        private System.Windows.Forms.GroupBox grbNewTask;
        private System.Windows.Forms.Panel panPending;
        private System.Windows.Forms.Label lblPending;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtTask;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblDone;
        private System.Windows.Forms.ListBox lstDone;
        private System.Windows.Forms.ContextMenuStrip mnuPending;
        private System.Windows.Forms.ToolStripMenuItem detallesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem hechaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem eliminarToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip mnuDone;
        private System.Windows.Forms.ToolStripMenuItem detallesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem pendienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem eliminarToolStripMenuItem1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        public System.Windows.Forms.ToolStripStatusLabel sbStatus;
    }
}

