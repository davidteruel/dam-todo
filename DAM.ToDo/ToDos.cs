﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;

namespace DAM.ToDo
{
    public class ToDos
    {
        public List<ToDo> lst;

        public ToDos()
        {
            /*ToDoData.Select() = lst;*/
        }

        public ToDo[] GetPending()
        {
            ToDo[] arrayToDos = new ToDo[lst.Count];
            int contador = 0;

            foreach (ToDo i in lst)
            {
                if (!i.Done.HasValue)
                {
                    arrayToDos[contador] = i;
                    contador++;
                }
            }

            return arrayToDos;
        }

        public ToDo[] GetDone()
        {
            ToDo[] arrayToDosDone = new ToDo[lst.Count];
            int contador = 0;

            foreach (ToDo i in lst)
            {
                if (i.Done.HasValue)
                {
                    arrayToDosDone[contador] = i;
                    contador++;
                }
            }

            return arrayToDosDone;
        }

        public void Add(string s)
        {
            ToDo tarea = new ToDo(s);
            lst.Add(tarea);
            /*ToDoData.Save();*/
        }

        public bool Remove(ToDo t)
        {

            return lst.Remove(t);
            ToDoData.Delete(t);
        }

        public void Do(ToDo t)
        {
            if (t.Done.HasValue)
            {
                Console.WriteLine("La tarea ya se realizó");
            }
            else
            {
                t.Done = DateTime.Now;
            }
            /*ToDoData.Save();*/
        }

        public void UnDo(ToDo t)
        {

            if (t.Done.HasValue)
            {
                t.Done = null;
            }
            else
            {
                Console.WriteLine("La tarea no se ha realizado todavía.");
            }
            /*ToDoData.Save();*/
        }
    }
}
