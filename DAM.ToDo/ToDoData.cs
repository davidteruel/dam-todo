﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace DAM.ToDo
{
    public static class ToDoData
    {
        //Recogemos la ruta del archivo de configuración
        private static string DataPath = System.Configuration.ConfigurationManager.AppSettings["DataPath"];

        public static void Save(ToDo t)
        {
            string fileName = t.Created.ToString("yyyyMMdd_hhmmss_FFFFFFF");
            string path = DataPath + fileName;

            using (FileStream fs = File.Create(path))
            {

            }


            using (StreamWriter sw = File.CreateText(path))
            {
                sw.WriteLine(t.Task);
                sw.WriteLine(t.Priority.GetHashCode());

                if(t.Done.HasValue)
                {
                    sw.WriteLine(t.Done.Value.ToString("yyyyMMdd_hhmmss_FFFFFFF"));
                }
                else
                {
                    sw.WriteLine();
                }
                if (t.Schedule.HasValue)
                {
                    sw.WriteLine(t.Schedule.Value.ToString("yyyyMMdd_hhmmss_FFFFFFF"));
                }
                else
                {
                    sw.WriteLine();
                }
                sw.WriteLine(t.Details);
            }
        }

        public static List<ToDo> Select()
        {

        }   

        public static void Delete(ToDo t)
        {

        }
    }
}
